/* Universidad de San Carlos de Guatemala
 * Laboratorio de electronica
 * Electronica 5, primer semestre de 2020
 * Estudiantes:                  Carnets:
 * Sergio Augusto León Urrutia   201700722
 * Huitzitzil Ajpo Noj Costop    201700376
 * Alex Ronaldo Mendoza Chonay   201709161   */


/******************** Librerias utilizadas **********************/
#include <Arduino.h>

#define   BUZZER   7
#define   PUSH_AZUL  11
#define   PUSH_AMARILLO  10
#define   PUSH_VERDE  9
#define   PUSH_ROJO  8

#define   LED_AZUL   2
#define   LED_AMARILLO   3
#define   LED_VERDE   4
#define   LED_ROJO   5

int melodia[] = {262, 196, 196, 220, 196, 0, 247, 262};
unsigned long duracionNotas[] = {4, 8, 8, 4, 4, 4, 4, 4};
int nivel_actual = 1;
int velocidad = 500;
const int nivel_maximo = 20;
int secuencia[nivel_maximo];
int secuencia_del_jugador[nivel_maximo];

/****************** inicio de métodos ***************************/

/****************** Médoto: Melodia de error ********************/
/* Este metodo genera una medolia de error para avisarle al usuario
 * que ha perdido. */
void melodiaError(){
   for(int i = 0; i < 8; i++){
      unsigned long duracionNota = 1000/duracionNotas[i];
      tone(BUZZER, melodia[i],duracionNotas);
      int pausaEntreNotas = duracionNota * 1.30;
      delay(pausaEntreNotas);
      noTone(BUZZER);
   }
}
/***************** Método: Secuencia de error ******************/
/* Este metodo da una alerta de que el jugador perdió, encendiendo
 * todos los leds del juego y reproduciendo una melodia de perdedor*/
void secuenciaError(){
   digitalWrite(LED_AZUL, HIGH);
   digitalWrite(LED_AMARILLO, HIGH);
   digitalWrite(LED_VERDE, HIGH);
   digitalWrite(LED_ROJO, HIGH);
   melodiaError();
   delay(250);
   digitalWrite(LED_AZUL, LOW);
   digitalWrite(LED_AMARILLO, LOW);
   digitalWrite(LED_VERDE, LOW);
   digitalWrite(LED_ROJO, LOW);
   delay(250);
   nivel_actual = 1;
   velocidad = 500;
}

/********************* Método: Secuencia correcta ****************
 * Este método le informa al usuario que ha realizado bien la
 * secuencia del nivel actual, encendiendo todos los leds e 
 * incrementando el nivel y la velocidad del juego */
void secuenciaCorrecta(){
   if(nivel_actual < nivel_maximo);
     nivel_actual++;
     velocidad -= 50;
     digitalWrite(LED_AZUL, HIGH);
     digitalWrite(LED_AMARILLO, HIGH);
     digitalWrite(LED_VERDE, HIGH);
     digitalWrite(LED_ROJO, HIGH);
     delay(500);
}

/********************** Método: Mostrar secuencia *********************
 * Este métdo lee la secuencia que genera el programa al inicio,
 * recorriendo los valores del vector 'secuencia' desde la posicion
 * 0 hasta el nivel actual. */
void muestraSecuencia(){
   digitalWrite(LED_AZUL, LOW);
   digitalWrite(LED_AMARILLO, LOW);
   digitalWrite(LED_VERDE, LOW);
   digitalWrite(LED_ROJO, LOW);
   delay(500);
   for(int i = 0; i < nivel_actual; i++){
      if( secuencia[i] == LED_AZUL ){
         tone(BUZZER, 200);
         delay(200);
         noTone(BUZZER);
      }
      if( secuencia[i] == LED_AMARILLO ){
         tone(BUZZER, 300);
         delay(200);
         noTone(BUZZER);
      }
      if( secuencia[i] == LED_VERDE ){
         tone(BUZZER, 400);
         delay(200);
         noTone(BUZZER);
      }
      if( secuencia[i] == LED_ROJO ){
         tone(BUZZER, 500);
         delay(200);
         noTone(BUZZER);
      }
      digitalWrite(secuencia[i], HIGH);
      delay(velocidad);
      digitalWrite(secuencia[i], LOW);
      delay(200);
   }
}

/********************** Método: Leer secuencia *********************
 * Este método lee la secuencia del jugador. Cada vez que el jugador
 * presiona un boton, el programa realiza una comparacion de las 
 * posiciones de los vectores 'secuencia' y 'secuencia_del_jugador'
 * si las secuencias coinciden se realiza el método 'secuenciacorrecta'
 * de lo contrario se realiza el método 'secuenciaerror' */
void leeSecuencia(){
   int flag = 0;
   for(int i = 0; i < nivel_actual; i++){
      flag = 0;
      while(flag == 0){
         if(digitalRead(PUSH_ROJO) == LOW){
            digitalWrite(LED_ROJO, HIGH);
            tone(BUZZER, 500);
            delay(300);
            noTone(BUZZER);
            secuencia_del_jugador[i] = LED_ROJO;
            flag = 1;
            delay(200);
            if(secuencia_del_jugador[i] != secuencia[i]){
               secuenciaError();
               return;
            }
            digitalWrite(LED_ROJO, LOW);
         }
         if(digitalRead(PUSH_VERDE) == LOW){
            digitalWrite(LED_VERDE, HIGH);
            tone(BUZZER, 400);
            delay(300);
            noTone(BUZZER);
            secuencia_del_jugador[i] = LED_VERDE;
            flag = 1;
            delay(200);
            if(secuencia_del_jugador[i] != secuencia[i]){
               secuenciaError();
               return;
            }
            digitalWrite(LED_VERDE, LOW);
         }
         if(digitalRead(PUSH_AMARILLO) == LOW){
            digitalWrite(LED_AMARILLO, HIGH);
            tone(BUZZER, 300);
            delay(300);
            noTone(BUZZER);
            secuencia_del_jugador[i] = LED_AMARILLO;
            flag = 1;
            delay(200);
            if(secuencia_del_jugador[i] != secuencia[i]){
               secuenciaError();
               return;
            }
            digitalWrite(LED_AMARILLO, LOW);
         }
         if(digitalRead(PUSH_AZUL) == LOW){
            digitalWrite(LED_AZUL, HIGH);
            tone(BUZZER, 200);
            delay(300);
            noTone(BUZZER);
            secuencia_del_jugador[i] = LED_AZUL;
            flag = 1;
            delay(200);
            if(secuencia_del_jugador[i] != secuencia[i]){
               secuenciaError();
               return;
            }
            digitalWrite(LED_AZUL, LOW);
         }
      }
   }
   secuenciaCorrecta();
}

/********************** Método: Generador de secuencia *********************
 * Este método genera una secuencia aleatoria para llenar el vector 'secuencia'
 * este vector será el que ponga a prueba las habilidades del jugador */
void generador_secuencia(){
   randomSeed(millis());
   for(int i = 0; i < nivel_maximo; i++){
      secuencia[i] = random(2,6);
   }
    for (int i = 1; i <= 6; i++) {
   digitalWrite(LED_ROJO , HIGH);
   delay(100);
   digitalWrite(LED_ROJO , LOW);
   digitalWrite(LED_VERDE , HIGH);
   delay(100);
   digitalWrite(LED_VERDE, LOW);
   digitalWrite(LED_AMARILLO , HIGH);
   delay(100);
   digitalWrite(LED_AMARILLO, LOW);
   digitalWrite(LED_AZUL , HIGH);
   delay(100);
   digitalWrite(LED_AZUL , LOW);
 }  
}

/************************* Método: Setup *********************
 * Este método realiza las configuraciones iniciales en la placa
 * adafruit metro (adafruit/arduino), al ser configuraciones,
 * solo se realizan una vez, al conectar el microcontrolador a 
 * una fuente de poder. */
void setup(){
   pinMode(PUSH_ROJO, INPUT_PULLUP);
   pinMode(PUSH_VERDE, INPUT_PULLUP);
   pinMode(PUSH_AMARILLO, INPUT_PULLUP);
   pinMode(PUSH_AZUL, INPUT_PULLUP);
   pinMode(LED_AZUL, OUTPUT);
   pinMode(LED_AMARILLO, OUTPUT);
   pinMode(LED_VERDE, OUTPUT);
   pinMode(LED_ROJO, OUTPUT);
}

/********************** Método: Loop ***********************
 * Este método es un metodo infinito, que se encarga de que
 * el microcontrolador este ejecutando procesos todo el tiempo,
 * y dentro de este método va el programa principal que ejecutará
 * el microcontrolador. */
void loop(){
   if(nivel_actual == 1){
      generador_secuencia();
      muestraSecuencia();
      leeSecuencia();
   }
   if(nivel_actual != 1){
      muestraSecuencia();
      leeSecuencia();
   }
}